import { useState } from "react";
const useInput = (defaultValue) => {
  const [value, setValue] = useState(defaultValue);
  return {
    value,
    onChange: setValue,
  };
};

export default useInput;
