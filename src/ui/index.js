import React, { useEffect, useRef } from "react";
import spine from "@byhuz/spine-ts/build/spine-player";
import "./style.css";
import Controls from "./Controls";
import useInput from "../hooks/useInput";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

const SKELETONS = [
  {
    name: "powerup",
    jsonUrl: "/powerup/powerup-pro.json",
    atlasUrl: "/powerup/powerup-pro.atlas",
  },
  {
    name: "raptor",
    jsonUrl: "/raptor/raptor-pro.json",
    atlasUrl: "/raptor/raptor-pro.atlas",
  },
];

export default (props) => {
  const selectedSkeletonIndex = useInput(1);
  const speed = useInput(1);
  const animations = useInput([]);
  const spinePlayerRef = useRef();

  useEffect(() => {
    const { name, atlasUrl, jsonUrl } = SKELETONS[selectedSkeletonIndex.value];

    spinePlayerRef.current = new spine.SpinePlayer(name, {
      atlasUrl,
      jsonUrl,
      showControls: false,
      success: onLoadSuccess,
    });
  }, [selectedSkeletonIndex.value]);

  const onLoadSuccess = () => {
    animations.onChange(
      spinePlayerRef.current?.animationState?.data.skeletonData.animations || []
    );
    spinePlayerRef.current.speed = speed.value;
  };

  const onAnimate = (animation) =>
    spinePlayerRef.current.setAnimation(animation);

  const onSpeedChange = (sp) => {
    speed.onChange(sp);
    spinePlayerRef.current.speed = sp;
  };

  return (
    <div className="app-container">
      <h1>{SKELETONS[selectedSkeletonIndex.value].name}</h1>
      <Grid container alignItems="flex-end" component={Paper}>
        <Grid item sm={8}>
          <div
            key={SKELETONS[selectedSkeletonIndex.value].name}
            id={SKELETONS[selectedSkeletonIndex.value].name}
            style={{ width: "100%", height: 480 }}
          ></div>
        </Grid>
        <Grid item sm={4}>
          <Controls
            skeletons={SKELETONS}
            selectedSkeletonIndex={selectedSkeletonIndex.value}
            onSelectedSkeletonChange={selectedSkeletonIndex.onChange}
            animations={animations.value}
            onAnimate={onAnimate}
            speedRange={[0.25, 2]}
            onSpeedChange={onSpeedChange}
            speed={speed.value}
          />
        </Grid>
      </Grid>
    </div>
  );
};
