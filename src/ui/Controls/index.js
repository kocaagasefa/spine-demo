import React from "react";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Slider from "@material-ui/core/Slider";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import "./style.css";

const Controls = ({
  skeletons,
  onSelectedSkeletonChange,
  animations,
  onAnimate,
  selectedSkeletonIndex,
  speedRange = [0.25, 2],
  onSpeedChange,
  speed,
}) => {
  return (
    <div className="controls-container">
      <Select
        label="Select Skeleton"
        className="select-skeleton"
        value={selectedSkeletonIndex}
        fullWidth
        onChange={(e) => onSelectedSkeletonChange(e.target.value)}
      >
        {(skeletons || []).map((skeleton, index) => (
          <MenuItem value={index} key={skeleton.name}>
            {skeleton.name}
          </MenuItem>
        ))}
      </Select>
      <div className="buttons-container">
        {animations.map((animation) => (
          <Button
            onClick={() => onAnimate(animation.name)}
            variant="contained"
            className="animation-button"
            key={animation.name}
          >
            {animation.name}
          </Button>
        ))}
      </div>
      <Box component="div" className="text-center slider-box">
        <span>Time Multiplier Val: {speed} </span>
        <Slider
          value={speed}
          onChange={(_, val) => onSpeedChange(val)}
          min={speedRange[0]}
          max={speedRange[1]}
          step={0.25}
        />
      </Box>
    </div>
  );
};

export default Controls;
